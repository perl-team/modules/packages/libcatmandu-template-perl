libcatmandu-template-perl (0.14-1) unstable; urgency=medium

  [ upstream ]
  * new release

  [ Debian Janitor ]
  * set upstream metadata fields:
    Bug-Database Bug-Submit Repository Repository-Browse
  * Bump debhelper from old 12 to 13.

  [ Jonas Smedegaard ]
  * update watch file: generalize version mangling
  * declare compliance with Debian Policy 4.6.1
  * update copyright info:
    + update coverage
    + use field Reference
  * use semantic newlines
    in long descriptions and copyright file License grants
  * update git-buildpackage config:
    + use DEP-14 git branches
    + enable automatic DEP-14 branch name handling
    + add usage config
  * simplify source helper script copyright-check

 -- Jonas Smedegaard <dr@jones.dk>  Tue, 27 Sep 2022 19:58:51 +0200

libcatmandu-template-perl (0.13-1) unstable; urgency=medium

  [ upstream ]
  * new release

  [ Salvatore Bonaccorso ]
  * Update Vcs-* headers for switch to salsa.debian.org

  [ gregor herrmann ]
  * debian/*: update GitHub URLs to use HTTPS.

  [ Jonas Smedegaard ]
  * copyright: update coverage
  * simplify rules;
    stop build-depend on cdbs dh-buildinfo
  * use debhelper compatibility level 12 (not 9);
    build-depend on debhelper-compat (not debhelper)
  * set Rules-Requires-Root: no
  * declare compliance with Debian Policy 4.5.0
  * annotate test-only build-dependencies
  * watch:
    + add versionmangle to enforce X.Y.X format
    + rewrite usage comment
  * build-depend on libmodule-build-perl
    (not libmodule-build-tiny-perl)

 -- Jonas Smedegaard <dr@jones.dk>  Thu, 02 Apr 2020 12:22:10 +0200

libcatmandu-template-perl (0.12-1) unstable; urgency=low

  * Initial Release.
    Closes: bug#877506.

 -- Jonas Smedegaard <dr@jones.dk>  Mon, 02 Oct 2017 11:59:30 +0200
